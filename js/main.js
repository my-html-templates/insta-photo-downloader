class InstaPhotoDownloader {
    constructor() {
        this.initEventHandlers()
        //this.loadPostPhotos()
    }

    get _self() { return this }

    initEventHandlers() {
        const self = this._self

        $('.search-input').keypress((e) => {
            if (e.which == 13) {
                alert("Thực hiện tìm kiếm...")
            }
        })
        
        $('.clear-input').click(self.clearInput())

        $('.photo-grid >.image > .image_hover-menu').on('click', () => {
            $('#photo-modal').addClass('is-active')
        })
    }

    loadPostPhotos() {
        alert("Loading post photo...")
        let imgUrlArr = []
        //imgUrlArr.map(i => ...)
    }

    loadTaggedPhotos() {
        let imgUrlArr = []
        //imgUrlArr.map(i => ...)
    }

    loadMorePhotos(photoNumber) {
        // ...
    }

    closeModal() {
        $('#photo-modal').removeClass('is-active')
    }
    findUser(event) {
        if (event.which == 13) {
            alert("Thực hiện tìm kiếm...")
        }
    }

    clearInput() {
        document.getElementById('search-input').value = ""
    }
}
