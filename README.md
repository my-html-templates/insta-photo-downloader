# Insta Photo Downloader

A template like Instagram, provide download option when hover a photo.

CSS: Bulma `>=0.8.0` and custom `SASS` files.

Font family: **SanFrancisco** (currently use), **HelveticaNowDisplay** + **HelveticaNowText** (experimental).

FontAwesome: `>=5.3.1`.

Javascript: Currently pure Javascript, with `ES6` coding style.